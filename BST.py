class Node:

    def __init__(self,data):
        self.data=data
        self.left=None
        self.right=None
        self.parent=None



class BST:

    def __init__(self):
        self.root=None
    # def __del__(self):
    #     print("dest")
    def insert(self,data):
        if self.root is None:
            self.root= Node(data)
        else:
            self._insertNode(self.root,data)

    def _insertNode(self,node,data):
        # print(node.data)
        if(data<node.data):
            if node.left is None:
                node.left= Node(data)
                node.left.parent= node
            else:
                self._insertNode(node.left,data)

        else:
            if node.right is None:
                node.right = Node(data)
                node.right = Node(data)
                node.right.parent = node
            else:
                self._insertNode(node.right,data)

    # parent = None;
    def delete(self,data):
        node = self.find(data)
        if node:
            self._deleteNode(node)

    def _deleteNode(self , node ):

        if node:
            parent = node.parent
            def noOfChild(node):
                if node.left is None and node.right is None:
                    return 0
                if node.left is not None and node.right is not None:
                    return  2
                if node .left is not None or node.right is not None:
                    return 1
            child= noOfChild(node)

            print(child," child")

            if child == 0:
                if parent is not None:
                    if parent.left is node:
                        parent.left = None
                    else:
                        parent.right=None

                    node.parent= None
                else:
                    self.root=None
            elif child == 1:
                left= node.left
                right= node.right
                if parent is not None:
                    if parent.left is node:
                        if left:
                            parent.left= left
                            left.parent= parent

                        else:
                            parent.left=right
                            right.parent=parent

                    elif parent.right is node:
                        if left:
                            parent.right= left
                            left.parent= parent

                        else:
                            parent.right=right
                            right.parent=parent
                    node.parent=None
                else:
                    if left:
                        self.root=left
                    else:
                        self.root = right
                    self.root.parent= None

            if child == 2:

                smallNode=node.right

                while smallNode.left:
                    smallNode = smallNode.left

                # print("data: ",smallNode.data)
                if parent is not None:

                    if parent.left is node:
                        parent.left= smallNode
                        smallNode.parent= parent
                        smallNode.left = node.left
                        # smallNode.right = node.right
                        node.parent= None

                    elif parent.right is node:
                        parent.right = smallNode
                        smallNode.parent =parent
                        smallNode.left= node.left
                        # smallNode.right= node.right
                        node.parent = None
                else:

                    self._deleteNode(smallNode)
                    original = self.root
                    self.root= smallNode
                    smallNode.left= original.left
                    smallNode.right = original.right



    def find(self,data):
        if self.root:
            return  self._findNode(self.root,data)
        else:
            return None

    def _findNode(self, node ,data):

        if data == node.data:
            return node
        elif data < node.data and node.left is not None:
            return self._findNode(node.left,data)
        elif data > node.data and node.right is not None:
            return self._findNode(node.right,data)

    def height(self):
        if self.root:
            return  self._findHeight(self.root,0)
    def _findHeight(self,node,count):
        if node is None:
            return count
        leftHeight= self._findHeight(node.left,count+1)
        rightHeight= self._findHeight(node.right,count+1)
        return max(leftHeight,rightHeight)

    def printInOrder(self,root):
        if root:
            self.printInOrder(root.left)
            print(root.data)
            self.printInOrder(root.right)

tree =  BST()

tree.insert(12)
tree.insert(10)
tree.insert(11)
tree.insert(5)
tree.insert(4)
tree.insert(1)
tree.insert(7)
tree.insert(6)
tree.insert(16)
tree.insert(14)
tree.insert(18)
tree.insert(20)
tree.insert(15)
tree.insert(17)
tree.insert(21)
tree.insert(22)

tree.delete(12)
# print(tree.find(9))
# print("h: ",tree.height())
tree.printInOrder(tree.root)
print("root ",tree.root.data)
# print()
# print(tree.root.right.left.data)