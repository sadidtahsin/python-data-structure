class Node:
    def __init__(self,data):
        self.data= data
        self.next= None


class LinkedList:
    def __init__(self):
        self.head = None

    def append(self,data):
        newNode= Node(data)
        if self.head is None:
            self.head=newNode
            return

        lastNode = self.head
        while lastNode.next:
            lastNode = lastNode.next
        lastNode.next=newNode

    def prepand(self,data):
        newNode = Node(data)
        if self.head is None:
            self.head= newNode
        else:
            newNode.next=self.head
            self.head=newNode

    def insertAfterNode(self,previousNode,data):
        if not previousNode:
            print("Not in the list")
            return
        newNode= Node(data)
        newNode.next=previousNode.next
        previousNode.next=newNode



    def printList(self):
        currentNode= self.head
        while currentNode:
            print(currentNode.data)
            currentNode= currentNode.next

    def has_cycles(self):
        """Floyd's Cycle-Finding Algorithm"""

        slow, fast = self.head, self.head

        while fast is not None and fast.next is not None:
            slow, fast = slow.next, fast.next.next

            if slow == fast:
                return True

        return False

lists= LinkedList()
lists.append("A")
lists.append("B")
lists.append("C")
lists.append("D")
lists.prepand("S")
lists.prepand("T")
lists.append("E")
lists.insertAfterNode(lists.head.next,"U")
lists.printList()

print(lists.has_cycles())
