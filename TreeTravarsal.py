class Node:
    def __init__(self,data):
        self.data = data
        self.left= None
        self.right = None


class TreeTravarsal:

    def __init__(self,data):
        self.root= Node(data)

    def printPreOrder(self,root):
        if root:
            print(root.data)
            self.printPreOrder(root.left)
            self.printPreOrder(root.right)

    def printInOrder(self,root):
        if root:
            self.printInOrder(root.left)
            print(root.data)
            self.printInOrder(root.right)

    def printPostOrder(self,root):
        if root:
            self.printPostOrder(root.left)
            self.printPostOrder(root.right)
            print(root.data)

tree  = TreeTravarsal(1)
tree.root.left = Node(2)
tree.root.right = Node(3)
tree.root.left.left = Node(4)
tree.root.left.right = Node(5)
tree.root.right.left = Node(6)
tree.root.right.right = Node(7)

tree.printPreOrder(tree.root)
print("________________")
tree.printInOrder(tree.root)
print("________________")
tree.printPostOrder(tree.root)