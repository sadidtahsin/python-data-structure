class Node:
    def __init__(self,data):
        self.data = data
        self.next = None
        self.previous = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def append(self,data):
        newNode = Node(data)
        if self.head is None:
            self.head= newNode
            self.tail= newNode
            return
        # lastNode= self.head
        # while lastNode.next:
        #     lastNode =lastNode.next
        # newNode.previous=lastNode
        # lastNode.next = newNode
        # self.tail=newNode
        newNode.previous=self.tail
        self.tail.next=newNode
        self.tail= newNode


    def preappend(self,data):
        newNode = Node(data)
        if self.head is None:
            self.head= newNode
            self.tail= newNode
            return
        newNode.next=self.head
        self.head.previous= newNode
        self.head= newNode

    def appendAfterNode(self,prv,data):
        newNode=  Node(data)
        # print("test ",prv)
        if not prv:
            print("Cannot Append")
            return
        temp = prv.next
        print("test", temp.data)
        newNode.previous=prv
        newNode.next=temp
        prv.next=newNode
        temp.previous=newNode

    def printListAscending(self):
        node = self.head

        while node:
            # print(node.data," -- " ,node, " -- ",node.prevoius," -- ",node.next )
            print(node.data)
            node= node.next

    def printListDescending(self):
        prvNode = self.tail
        while prvNode:
            print(prvNode.data)
            prvNode=prvNode.previous



lists= DoublyLinkedList()
lists.append(2)
lists.append(3)
lists.append(4)
lists.preappend(1)
lists.preappend(-1)
lists.appendAfterNode(lists.head,0)



lists.printListAscending()
print("---------------")
lists.printListDescending()


