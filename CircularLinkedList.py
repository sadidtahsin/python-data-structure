class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

class CircularLinkedList:
    def __init__(self):
        self.head = None

    def append(self,data):
        newNode =  Node(data)
        if self.head is None:
            self.head = newNode
            self.head.next=newNode
            return
        tempNode = self.head
        while tempNode.next is not self.head:
            tempNode= tempNode.next

        tempNode.next=newNode
        newNode.next=self.head

    def insertAfterNode(self,previousNode,data):
        if not previousNode:
            print("Not in the list")
            return
        newNode= Node(data)
        newNode.next=previousNode.next
        previousNode.next=newNode

    def printList(self):
        i=1
        flag =True
        node= self.head


        while True:
            print(node.data)
            node= node.next
            if node == self.head:
                break;
def has_cycle(head):
    """Floyd's Cycle-Finding Algorithm"""

    slow, fast = head, head

    while fast is not None and fast.next is not None:
        slow, fast = slow.next, fast.next.next

        if slow == fast:
            return True

    return False


clist= CircularLinkedList()
clist.append(4)
clist.append(5)
clist.append(8)
clist.append(9)
clist.insertAfterNode(clist.head.next,6)
clist.printList()

print(has_cycle(clist.head))